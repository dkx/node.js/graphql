import {RelationshipConfiguration} from '../configuration';
import {ResultMapper} from './result-mapper';
import {RequestContext} from '../request-context';


export class RelationshipFactory<T>
{
	constructor(
		private readonly context: RequestContext,
		private readonly mapper: ResultMapper,
	) {}

	public create<TRelationship>(relationship: RelationshipConfiguration<TRelationship>, data: any): any
	{
		return this.mapper.mapType(this.context, relationship.configuration, data);
	}
}
