import {RequestContext} from '../request-context';
import {ObjectConfiguration} from '../configuration';


export interface ResultMapper
{
	map(ctx: RequestContext, data: any): any;

	mapType<T>(ctx: RequestContext, configuration: ObjectConfiguration<T>, data: any): any;
}
