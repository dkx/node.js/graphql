import {ResultMapper} from './result-mapper';
import {Type} from '../type';
import {RequestContext} from '../request-context';
import {ObjectConfiguration} from '../configuration';
import {RelationshipFactory} from './relationship-factory';


export class EntityResultMapper<T> implements ResultMapper
{
	constructor(
		private readonly type: Type<T>,
	) {}

	public map(ctx: RequestContext, data: any): any
	{
		return this.mapType(ctx, ctx.configuration.getType(this.type), data);
	}

	public mapType<TInner>(ctx: RequestContext, configuration: ObjectConfiguration<TInner>, data: any): any
	{
		const relationshipFactory = new RelationshipFactory<TInner>(ctx, this);

		if (Array.isArray(data)) {
			return data.map(item => configuration.create(item, relationshipFactory));
		}

		return configuration.create(data, relationshipFactory);
	}
}
