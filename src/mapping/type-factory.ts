import {RelationshipFactory} from './relationship-factory';


export interface TypeFactory<T>
{
	create(data: any, relationshipFactory: RelationshipFactory<T>): T;
}
