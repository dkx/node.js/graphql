import {TypeFactory} from './type-factory';
import {TypedObjectConfiguration} from '../configuration';
import {RelationshipFactory} from './relationship-factory';


export class DefaultTypeFactory<T> implements TypeFactory<T>
{
	constructor(
		private readonly configuration: TypedObjectConfiguration<T>,
	) {}

	public create(data: any, relationshipFactory: RelationshipFactory<T>): T
	{
		const entity = new this.configuration.type();

		this.configuration.fields.forEach(field => {
			if (typeof data[field.definition] !== 'undefined') {
				let value = data[field.definition];
				if (typeof field.factory !== 'undefined') {
					value = field.factory.transform(value);
				}

				entity[field.name as keyof T] = value;
			}
		});

		this.configuration.relationships.forEach(relationship => {
			if (data[relationship.definition] === null) {
				entity[relationship.name as keyof T] = null as unknown as T[keyof T];
			} else if (typeof data[relationship.definition] !== 'undefined') {
				entity[relationship.name as keyof T] = relationshipFactory.create(relationship, data[relationship.definition]);
			}
		});

		return entity;
	}
}
