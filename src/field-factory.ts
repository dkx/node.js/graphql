export interface FieldFactory<T, TInput = any>
{
	transform(value: TInput): T;
}
