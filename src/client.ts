import {QueryTemplate} from './query-template';
import {Configuration} from './configuration';
import {DefaultHttpRequestFactory, EndpointProvider, HttpClient, HttpRequestFactory} from './http';
import {HttpErrorsList, HttpError} from './errors';
import {RequestContext} from './request-context';
import {Result} from './result';


export declare interface ClientOptions
{
	httpRequestFactory?: HttpRequestFactory,
}

export declare interface QueryOptions
{
	operationName?: string,
	variables?: {[key: string]: any},
}

export class Client
{
	private readonly httpRequestFactory: HttpRequestFactory;

	constructor(
		private readonly http: HttpClient,
		private readonly configuration: Configuration,
		private readonly endpointProvider: EndpointProvider,
		options: ClientOptions = {},
	) {
		this.httpRequestFactory = options.httpRequestFactory ?? new DefaultHttpRequestFactory();
	}

	public async request<T = any>(template: QueryTemplate, options?: QueryOptions): Promise<Result<T>>
	{
		const ctx = new RequestContext(this.configuration);
		const query = template.process(ctx);
		let variables: Record<string, any> = {};

		ctx.variables.forEach((value, key) => {
			variables[key] = value;
		});

		if (typeof options?.variables !== 'undefined') {
			variables = {...variables, ...options.variables};
		}

		const body: any = {
			query,
		};

		if (typeof options?.operationName === 'string') {
			body.operationName = options.operationName;
		}

		if (Object.keys(variables).length > 0) {
			body.variables = variables;
		}

		const response = await this.http.request(this.httpRequestFactory.createRequest(this.endpointProvider.getUrl(), JSON.stringify(body)));
		const data = JSON.parse(response);

		if (typeof data.errors !== 'undefined' && Array.isArray(data.errors)) {
			const errors = data.errors.map((e: any) => new HttpError(e.message, e));
			throw new HttpErrorsList(errors);
		}

		return new Result<T>(ctx, data.data);
	}
}
