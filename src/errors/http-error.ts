export class HttpError extends Error
{
	constructor(
		message: string,
		public readonly data: any,
	)
	{
		super(message);
		Object.setPrototypeOf(this, HttpError.prototype);
	}
}
