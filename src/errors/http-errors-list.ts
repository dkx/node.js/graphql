import {HttpError} from './http-error';


export class HttpErrorsList extends Error
{
	constructor(
		public readonly errors: Array<HttpError>,
	)
	{
		super(errors.map(e => e.message).join('\n'));
		Object.setPrototypeOf(this, HttpErrorsList.prototype);
	}
}
