import {Configuration} from './configuration';


export class RequestContext
{
	public readonly variables: Map<string, any> = new Map();

	constructor(
		public readonly configuration: Configuration,
	) {}

	public addVariable<T>(name: string, value: T): void
	{
		this.variables.set(name, value);
	}
}
