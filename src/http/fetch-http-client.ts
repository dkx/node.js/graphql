import {HttpClient} from './http-client';
import {HttpRequest} from './http-request';


export class FetchHttpClient implements HttpClient
{
	public request(request: HttpRequest): Promise<string>
	{
		return fetch(request.url, {
			method: 'POST',
			body: request.body,
			headers: {
				...request.headers,
				'Content-Type': 'application/json',
			},
		}).then(async res => res.text());
	}
}
