export class HttpRequest
{
	constructor(
		public readonly url: string,
		public readonly body: string,
		public readonly headers: Record<string, string> = {},
	) {}
}
