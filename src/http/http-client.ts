import {HttpRequest} from './http-request';


export interface HttpClient
{
	request(request: HttpRequest): Promise<string>;
}
