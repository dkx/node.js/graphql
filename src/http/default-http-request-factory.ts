import {HttpRequestFactory} from './http-request-factory';
import {HttpRequest} from './http-request';


export class DefaultHttpRequestFactory implements HttpRequestFactory
{
	public createRequest(url: string, body: string): HttpRequest
	{
		return new HttpRequest(url, body);
	}
}
