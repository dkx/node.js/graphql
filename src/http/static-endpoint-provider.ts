import {EndpointProvider} from './endpoint-provider';


export class StaticEndpointProvider implements EndpointProvider
{
	constructor(
		private readonly url: string,
	) {}

	public getUrl(): string
	{
		return this.url;
	}
}
