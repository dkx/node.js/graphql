import {HttpRequest} from './http-request';


export interface HttpRequestFactory
{
	createRequest(url: string, body: string): HttpRequest;
}
