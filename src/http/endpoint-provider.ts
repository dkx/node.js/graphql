export interface EndpointProvider
{
	getUrl(): string;
}
