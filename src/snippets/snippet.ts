import {RequestContext} from '../request-context';


export abstract class Snippet
{
	public abstract process(ctx: RequestContext): string;
}
