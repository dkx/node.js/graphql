import {Snippet} from './snippet';
import {RequestContext} from '../request-context';

export declare interface VariableDefinition
{
	definition: string,
	name?: string,
}

export type VariablesDefinition<T> = {[P in keyof T]-?: VariableDefinition};

export function variables<T>(definition: VariablesDefinition<T>, values: T): Variables<T>
{
	return new Variables<T>(definition, values);
}

export class Variables<T>
{
	private readonly mapping: Map<string, string> = new Map();

	constructor(
		private readonly definition: VariablesDefinition<T>,
		private readonly values: T,
	) {}

	public toDeclarations(): VariablesDeclarationsSnippet<T>
	{
		return new VariablesDeclarationsSnippet(this.definition, this.values, this.mapping);
	}

	public toArguments(): VariablesArgumentsSnippet<T>
	{
		return new VariablesArgumentsSnippet(this.definition, this.values, this.mapping);
	}
}

export class VariablesDeclarationsSnippet<T> extends Snippet
{
	constructor(
		private readonly definition: VariablesDefinition<T>,
		private readonly values: T,
		private readonly mapping: Map<string, string>,
	) {
		super();
	}

	public process(ctx: RequestContext): string
	{
		const fields: Array<string> = [];

		for (let key in this.definition) {
			if (this.definition.hasOwnProperty(key) && typeof this.values[key] !== 'undefined') {
				const name = typeof this.definition[key].name === 'string' ?
					this.definition[key].name as string :
					`p${ctx.variables.size}`;

				ctx.addVariable(name, this.values[key]);
				this.mapping.set(key, name);
				fields.push(`$${name}: ${this.definition[key].definition}`);
			}
		}

		return fields.join(', ');
	}
}

export class VariablesArgumentsSnippet<T> extends Snippet
{
	constructor(
		private readonly definition: VariablesDefinition<T>,
		private readonly values: T,
		private readonly mapping: Map<string, string>,
	) {
		super();
	}

	public process(ctx: RequestContext): string
	{
		const fields: Array<string> = [];

		for (let key in this.definition) {
			if (this.definition.hasOwnProperty(key) && typeof this.values[key] !== 'undefined') {
				const name = this.mapping.get(key);
				if (typeof name === 'undefined') {
					throw new Error('VariablesSnippet: toArguments() must be called after toDeclaration()')
				}

				fields.push(`${key}: $${name}`);
			}
		}

		return fields.join(', ');
	}
}
