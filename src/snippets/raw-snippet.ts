import {Snippet} from './snippet';
import {RequestContext} from '../request-context';


export function raw(str: string): RawSnippet
{
	return new RawSnippet(str);
}

export class RawSnippet extends Snippet
{
	constructor(
		private readonly str: string,
	) {
		super();
	}

	public process(ctx: RequestContext): string
	{
		return this.str;
	}
}
