import {Type} from '../type';
import {Snippet} from './snippet';
import {RequestContext} from '../request-context';
import {RelationshipInclusion, ObjectConfiguration} from '../configuration';


declare type Relationships = {[parent: string]: Relationships};

export function allFields<T>(type: Type<T>, relationships: Array<string> = []): FieldsSnippet<T>
{
	return new FieldsSnippet<T>(type, relationships);
}

export class FieldsSnippet<T> extends Snippet
{
	constructor(
		private readonly type: Type<T>,
		private readonly relationships: Array<string> = [],
	) {
		super();
	}

	public process(ctx: RequestContext): string
	{
		return this.processType(
			ctx,
			ctx.configuration.getType(this.type),
			FieldsSnippet.parseRelationships(this.relationships),
		);
	}

	private processType<TInner>(ctx: RequestContext, configuration: ObjectConfiguration<TInner>, relationships: Relationships): string
	{
		const fields: Array<string> = [];

		configuration.fields.forEach(field => {
			fields.push(field.definition);
		});

		configuration.relationships.forEach(relationship => {
			if (relationship.include === RelationshipInclusion.Always || typeof relationships[relationship.name as string] !== 'undefined') {
				const innerRelationships: Relationships = relationships[relationship.name as string] ?? {};
				fields.push(`${relationship.name} {${this.processType(ctx, relationship.configuration, innerRelationships)}}`);
			}
		});

		return fields.join(', ');
	}

	private static parseRelationships(all: Array<string>): Relationships
	{
		const result: Relationships = {};

		for (let relationship of all) {
			const parts = relationship.split('.');
			let root = result;

			for (let part of parts) {
				if (typeof root[part] === 'undefined') {
					root[part] = {};
				}

				root = root[part];
			}
		}

		return result;
	}
}
