import {ObjectConfiguration} from './object-configuration';
import {FieldConfiguration, FieldConfigurationOptions} from './field-configuration';
import {RelationshipConfiguration, RelationshipConfigurationOptions} from './relationship-configuration';
import {RelationshipFactory, TypeFactory} from '../mapping';


export class ArbitraryObjectConfiguration<T> implements ObjectConfiguration<T>
{
	public readonly fields: Set<FieldConfiguration> = new Set();

	public readonly relationships: Set<RelationshipConfiguration<any>> = new Set();

	constructor(
		private readonly factory: TypeFactory<T>,
	) {}

	public create(data: any, relationshipFactory: RelationshipFactory<T>): T
	{
		return this.factory.create(data, relationshipFactory);
	}

	public addField(selector: string, options?: FieldConfigurationOptions): this
	{
		this.fields.add(new FieldConfiguration(selector, options));
		return this;
	}

	public addRelationship<TRelationship>(selector: string, relationship: ObjectConfiguration<TRelationship>, options: RelationshipConfigurationOptions = {}): this
	{
		this.relationships.add(new RelationshipConfiguration(relationship, selector as string, options));
		return this;
	}
}
