import {Type} from '../type';
import {ObjectConfiguration} from './object-configuration';
import {TypedObjectConfiguration} from './typed-object-configuration';
import {ArbitraryObjectConfiguration} from './arbitrary-object-configuration';
import {TypeFactory} from '../mapping';


export class Configuration
{
	private readonly types: Set<ObjectConfiguration<any>> = new Set();

	public addType<T>(type: Type<T>): TypedObjectConfiguration<T>
	{
		const config = new TypedObjectConfiguration(type);
		this.types.add(config);
		return config;
	}

	public addArbitraryType<T>(factory: TypeFactory<T>): ArbitraryObjectConfiguration<T>
	{
		const config = new ArbitraryObjectConfiguration<T>(factory);
		this.types.add(config);
		return config;
	}

	public getType<T>(type: Type<T>): TypedObjectConfiguration<T>
	{
		for (let config of this.types) {
			if (config instanceof TypedObjectConfiguration && config.type === type) {
				return config;
			}
		}

		throw new Error(`GraphQL: missing configuration for type ${type.name}`);
	}
}
