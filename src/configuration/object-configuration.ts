import {FieldConfiguration} from './field-configuration';
import {RelationshipConfiguration} from './relationship-configuration';
import {RelationshipFactory} from '../mapping';


export interface ObjectConfiguration<T>
{
	readonly fields: Set<FieldConfiguration>;

	readonly relationships: Set<RelationshipConfiguration<any>>;

	create(data: any, relationshipFactory: RelationshipFactory<T>): T;
}
