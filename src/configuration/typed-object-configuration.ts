import {Type} from '../type';
import {FieldConfiguration, FieldConfigurationOptions} from './field-configuration';
import {RelationshipConfiguration, RelationshipConfigurationOptions} from './relationship-configuration';
import {TypeFactory, DefaultTypeFactory, RelationshipFactory} from '../mapping';
import {ObjectConfiguration} from './object-configuration';


export class TypedObjectConfiguration<T> implements ObjectConfiguration<T>
{
	public readonly fields: Set<FieldConfiguration> = new Set();

	public readonly relationships: Set<RelationshipConfiguration<any>> = new Set();

	private _factory: TypeFactory<T>|null = null;

	constructor(
		public readonly type: Type<T>,
	) {}

	public create(data: any, relationshipFactory: RelationshipFactory<T>): T
	{
		return this.getOrCreateFactory().create(data, relationshipFactory);
	}

	public addField<TField extends keyof T>(selector: TField, options?: FieldConfigurationOptions): this
	{
		this.fields.add(new FieldConfiguration(selector as string, options));
		return this;
	}

	public addRelationship<TField extends keyof T, TRelationship = T[TField]>(selector: TField, relationship: ObjectConfiguration<TRelationship>, options: RelationshipConfigurationOptions = {}): this
	{
		this.relationships.add(new RelationshipConfiguration(relationship, selector as string, options));
		return this;
	}

	public setFactory(factory: TypeFactory<T>): this
	{
		if (this._factory !== null) {
			throw new Error('Factory is already set');
		}

		this._factory = factory;

		return this;
	}

	private getOrCreateFactory(): TypeFactory<T>
	{
		if (this._factory === null) {
			this._factory = new DefaultTypeFactory(this);
		}

		return this._factory;
	}
}
