import {ObjectConfiguration} from './object-configuration';


export enum RelationshipInclusion
{
	WhenRequested,
	Always,
}

export declare interface RelationshipConfigurationOptions
{
	definition?: string,
	include?: RelationshipInclusion,
}

export class RelationshipConfiguration<TRelationship>
{
	public readonly definition: string;

	public readonly include: RelationshipInclusion;

	constructor(
		public readonly configuration: ObjectConfiguration<TRelationship>,
		public readonly name: string,
		options?: RelationshipConfigurationOptions
	) {
		this.definition = options?.definition ?? this.name;
		this.include = options?.include ?? RelationshipInclusion.WhenRequested;
	}
}
