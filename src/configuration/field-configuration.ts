import {FieldFactory} from '../field-factory';


export declare interface FieldConfigurationOptions
{
	definition?: string,
	factory?: FieldFactory<any>,
}

export class FieldConfiguration
{
	public readonly definition: string;

	public readonly factory?: FieldFactory<any>;

	constructor(
		public readonly name: string,
		options?: FieldConfigurationOptions,
	) {
		this.definition = options?.definition ?? this.name;
		this.factory = options?.factory;
	}
}
