import {Snippet} from './snippets';
import {RequestContext} from './request-context';


export function gql(strings: TemplateStringsArray, ...args: Array<any>): QueryTemplate
{
	return new QueryTemplate(strings, args);
}

export class QueryTemplate
{
	constructor(
		public readonly strings: TemplateStringsArray,
		public readonly args: Array<any>,
	) {}

	public process(ctx: RequestContext): string
	{
		const parts: Array<string> = [];

		this.strings.forEach((str, i) => {
			let arg = this.args[i] ?? '';
			if (arg instanceof Snippet) {
				arg = arg.process(ctx);
			}

			parts.push(str);
			parts.push(arg);
		});

		return parts.join('');
	}
}
