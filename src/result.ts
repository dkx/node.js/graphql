import {EntityResultMapper, ResultMapper} from './mapping';
import {RequestContext} from './request-context';
import {Type} from './type';


export class Result<T>
{
	constructor(
		private readonly ctx: RequestContext,
		private readonly data: T,
	) {}

	public json(): T
	{
		return this.data;
	}

	public map<TResult>(map: (data: T) => TResult): Result<TResult>
	{
		return new Result<TResult>(this.ctx, map(this.data));
	}

	public use<TResult = any>(mapper: ResultMapper): TResult
	{
		return mapper.map(this.ctx, this.data);
	}

	public result<TType, TResult = any>(type: Type<TType>): TResult
	{
		return this.use<TResult>(new EntityResultMapper(type));
	}
}
