# Configuration

```typescript
import {Configuration} from '@dkx/graphql';

class Book
{
    public readonly id!: string;
    public readonly title!: string;
    public readonly author!: User;
}

config.addType(Book)
    .addField('id')
    .addField('title')
    .addRelationship('author', User);
```

## Custom field definition

```typescript
import {Configuration} from '@dkx/graphql';

class Book
{
    public readonly thumbnail!: string;
    public readonly picture!: string;
}

config.addType(Book)
    .addField('thumbnail', {definition: 'photo(size: "100px")'})
    .addField('picture', {definition: 'photo(size: "2500px")'});
```

## Custom type factory

Type can be constructed manually by implementing custom `TypeFactory`:

```typescript
import {TypeFactory, CreateRelationship} from '@dkx/graphql';

class Money
{
    constructor(
        public readonly currency: string,
        public readonly amount: number,
    ) {}
}

class MoneyTypeFactory implements TypeFactory<Money>
{
    public create(data: any, relationshipFactory: RelationshipFactory<Money>): Money
    {
        return new Money(data.currency, data.amount);
    }
}
```

Now custom factory can be used:

```typescript
config.addType(Money)
    .setFactory(new MoneyTypeFactory())
    .addField('currency')
    .addField('amount');
```

## Always included relationship

```typescript
import {RelationshipInclusion} from '@dkx/graphql';

class Book
{
    public readonly price!: Money;
}

config.addType(Book)
    .type.addRelationship('price', Money, {
        include: RelationshipInclusion.Always,
    });
```

## Adding arbitrary object types

Arbitrary object types can not be automatically mapped, so you must first create custom type factory (see above).

```typescript
declare interface Money
{
    amount: number,
    currency: string,
}

config.addArbitraryType<Money>(new MoneyFactory())
    .addField('amount')
    .addField('currency');
```
