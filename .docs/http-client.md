# HTTP Client

## Built in clients

* `FetchHttpClient`: using `fetch` browser function

## Writing custom HTTP client

Let's write custom HTTP client for angular:

```typescript
import {HttpClient as HttpClientInterface, HttpRequest} from '@dkx/graphql';
import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';

@Injectable()
class AngularHttpClient implements HttpClientInterface
{
    constructor(
        private readonly http: HttpClient,
    ) {}

    public request(request: HttpRequest): Promise<string>
    {
        return this.http.post(request.url, request.body, {
            responseType: 'text',
            headers: new HttpHeaders({
                ...request.headers,
                'Content-Type': 'application/json',
            }),
        }).toPromise();
    }
}
```

## Custom HTTP request factory

You can also write custom HTTP request factory which allows you to modify all GraphQL calls, e.g. by adding Authorization header.

```typescript
import {HttpRequestFactory, HttpRequest} from '@dkx/graphql';

class AuthorizedHttpRequestFactory implements HttpRequestFactory
{
    constructor(
        private readonly token: string,
    ) {}

    public createRequest(url: string, body: string): HttpRequest
    {
        return new HttpRequest(
            url,
            body,
            {
                Authorization: this.token,
            },
        );
    }
}
```

It can now be used in a client:

```typescript
import {Client} from '@dkx/graphql';

const client = new Client(http, config, endpoint, {
    httpRequestFactory: new AuthorizedHttpRequestFactory('my-auth-token'),
});
```
