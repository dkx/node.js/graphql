# Errors

Every error returned from server is automatically transformed into `HttpError`. List of errors is wrapped inside of `HttpErrorsList`.

```typescript
import {gql, HttpErrorsList, HttpError} from '@dkx/graphql';

try {
    await client.request(gql`invalid query...`);
} catch (e: unknown) {
    if (e instanceof HttpErrorsList) {
        alert('GraphQL server returned errors: \n' + e.message);
        e.errors.forEach(error => {
            console.error(error);
        });
    }
    
    throw e;
}
```
