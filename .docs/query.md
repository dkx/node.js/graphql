# Query

```typescript
import {gql} from '@dkx/graphql';

const query = gql`
    query {
        getBooks {
            id,
            name
        }
    }
`;

const response = await client.request(query);
const data = response.json();
// output: {getBooks: Array<{id: string, name: string}>}
```

## Extract data from result

```typescript
const response = await client.request(query);
const data = response.map(data => data.getBooks);

// output: Array<{id: string, name: string}>
```

## Map result to entity class

This feature depends on types [configuration](configuration.md) and you may first need to extract the result object (see above).

See also how to write [custom mappers](custom-mapper.md).

```typescript
import {EntityResultMapper} from '@dkx/graphql';

const response = await client.request(query);
const data = response
    .map(data => data.getBooks)
    .result(Book);

// output: Array<Book>
```
