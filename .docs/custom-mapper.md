# Custom mapper

Let's create `PaginatedResultMapper` that maps paginated data into `PaginatedData` class:

```typescript
import {ResultMapper, EntityResultMapper, Type} from '@dkx/graphql';

class PaginatedData<T>
{
    constructor(
        public readonly data: Array<T>,
        public readonly page: number,
        public readonly totalCount: number,
        public readonly itemsPerPage: number,
    ) {}
}

class PaginatedResultMapper<T> implements ResultMapper
{
    constructor(
        private readonly type: Type<T>,
    ) {}

    public map(ctx: RequestContext, data: any): any
    {
        // todo: validate that data contains paginated data

        const innerMapper = new EntityResultMapper(this.type);

        return new PaginatedData(
            innerMapper.map(ctx, data.data),
            data.page,
            data.totalCount,
            data.itemsPerPage,
        );
    }

    public mapType<TInner>(ctx: RequestContext, configuration: TypeConfiguration<TInner>, data: any): any
    {
        // map all other data with default mapper
        return new EntityResultMapper(configuration.type).map(ctx, data);
    }
}
```

Now we can use this mapper in our queries:

```typescript
import {gql} from '@dkx/graphql';

const result = await client.request<PaginatedData<Book>>(gql`
    query {
        getPaginatedBooks {
            data {
                id,
                name,
            },
            page,
            totalCount,
            itemsPerPage,
        }
    }
`, {
    extract: data => data.getPaginatedBooks,
    mapper: new PaginatedResultMapper(Book),
});

const paginatedData = result
    .map(data => data.getPaginatedBooks)
    .use(new PaginatedResultMapper(Book));
```
