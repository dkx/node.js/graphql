# Endpoint provider

There is one built-in endpoint provider you can use - `StaticEndpointProvider`. URL from this endpoint never change.

```ts
import {StaticEndpointProvider} from '@dkx/graphql';

const endpoint = new StaticEndpointProvider('https://example.com/graphql');
```

## Custom endpoint provider

You can write custom endpoint provider by implementing `EndpointProvider` interface.

Example of `DynamicEndpointProvider`:

```ts
import {EndpointProvider} from '@dkx/graphql';

class DynamicEndpointProvider implements EndpointProvider
{
	public getUrl(): string
    {
    	return getGraphQlUrlSomehow();
    }
}
```
