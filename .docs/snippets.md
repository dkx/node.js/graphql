# Snippet

Snippets are special building blocks for easier construction of your queries. They are the parts of code in curly braces
within your queries.

## All fields snippet

```typescript
import {allFields} from '@dkx/graphql';
```

Get root fields:

```typescript
allFields(Book);
// expands to: id, title
```

Get root fields with related type:

```typescript
allFields(Book, ['author', 'chapters']);
// expands to: id, title, author {id, email}, chapters {id, title, content}
```

Get root fields with nested related types:

```typescript
allFields(Book, ['chapters.author.role']);
// expands to: id, title, chapters {id, title, content, author {id, email, role {id, name}}}
```

## Variables snippet

```typescript
import {variables} from '@dkx/graphql';

declare interface GetBooksQueryVariables
{
    page?: int,
    limit?: int,
    query?: string,
}

const values: GetBooksQueryVariables = {
    page: 0,
    limit: 20,
};

const vars = variables<GetBooksQueryVariables>({
    page: {definition: 'Int'},
    limit: {definition: 'Int'},
    query: {definition: 'String'},
}, values);

vars.toDeclarations();
// output: $p0: Int, $p1: Int

vars.toArguments();
// output: page: $p0, limit: $p1
```

Inside a query:

```typescript
import {gql} from '@dkx/graphql';

const query = gql`
    query(${vars.toDeclarations()}) {
        getBooks(${vars.toArguments()}) {
            id,
            name,
        }
    }
`;
```

Variables snippet automatically omits any variables which are not present in the `values` object. It makes it useful when
your GraphQL server needs to distinguish between missing variable and nulls.

## Raw snippet

```typescript
import {raw} from '@dkx/graphql';

raw('id, name');
// output: id, name 
```

## Writing custom snippet

Let's write a snippet that wraps `allFields` snippets and adds pagination fields.

See also how to [write custom result mapper](custom-mapper.md) that maps response data into paginated data.

```typescript
import {Snippet, Type, allFields} from '@dkx/graphql';

class PaginatedFieldsSnippet<T> extends Snippet
{
    constructor(
        private readonly type: Type<T>,
        private readonly relationships: Array<string> = [],
    ) {
        super();
    }

    public process(ctx: RequestContext): string
    {
        const fields = allFields(this.type, this.relationships);
        return `page, totalCount, itemsPerPage, data {${fields.process(ctx)}}`;
    }
}
```
