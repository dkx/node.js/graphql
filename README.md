# @dkx/graphql

Another GraphQL client... 

* Mapping to classes
* Fetching all fields without having to write them all the time
* Promise await/async only

## Installation

```bash
npm install --save @dkx/graphql
```

## Example

```typescript
import {Configuration, Client, FetchHttpClient, gql, allFields, EntityResultMapper, StaticEndpointProvider} from '@dkx/graphql';

class Book {
    public readonly id!: string;
    public readonly title!: string;
    public readonly author!: User;
}

class User {
    public readonly id!: string;
    public readonly email!: string;
}

const config = new Configuration();

const userConfig = config.addType(User)
    .addField('id')
    .addField('email');

config.addType(Book)
    .addField('id')
    .addField('title')
    .addRelationship('author', userConfig);

const http = new FetchHttpClient();
const endpoint = new StaticEndpointProvider('https://example.com/graphql');
const client = new Client(http, configuration, endpoint);

// fetch all books with authors
const result = await client.request<Array<Book>>(gql`
    query {
        getBooks {
            ${allFields(Book, ['author'])}
        }
    }
`);

// extract array of books from json and map to Book entity
const books = result
    .map(data => data.getBooks)
    .result(Book);
```

## Documentation

* [Configuration](.docs/configuration.md)
* [Query](.docs/query.md)
* [Snippets](.docs/snippets.md)
* [Custom mapper](.docs/custom-mapper.md)
* [Errors](.docs/errors.md)
* [HTTP client](.docs/http-client.md)
* [Endpoint provider](.docs/endpoint-provider.md)
