import {Client, allFields, gql, variables, HttpErrorsList, StaticEndpointProvider} from '../src';
import {MockHttpClient} from './mock-http-client';
import {
	APP_CONFIGURATION,
	Book,
	Role,
	User,
	Comment,
	Chapter,
	APP_DATE,
	GetUsersVariables,
	GetBookVariables,
} from './schema';


let client: Client;

beforeEach(() => {
	client = new Client(new MockHttpClient(), APP_CONFIGURATION, new StaticEndpointProvider('http://localhost'));
});

test('receive raw data', async () => {
	const data = await client.request(gql`
		query {
			getBooks {title}
		}
	`).then(response => response.json());

	expect(data).toStrictEqual({
		getBooks: [
			{title: 'Book_title'},
			{title: 'Book_title'},
		],
	});
});

test('receive raw book with allFields snippet', async () => {
	const data = await client.request(gql`
		query {
			getBook(id: "42") {${allFields(Book)}}
		}
	`).then(response => response.json());

	expect(data).toStrictEqual({
		getBook: {
			id: '42',
			title: 'Book_title',
			createdAt: APP_DATE.toISOString(),
			price: {
				amount: 1000,
				currency: 'CZK',
			},
		},
	});
});

test('transform errors to exceptions', async () => {
	const assert = expect(client.request(gql`
		query {
			getRole(id: "42") {${allFields(Book)}}
			getBook(id: "42")
		}
	`)).rejects;

	await assert.toBeInstanceOf(HttpErrorsList);
});

test('extract result', async () => {
	const response = await client.request(gql`
		query {
			getBook(id: "42") {id}
		}
	`);

	const data = response
		.map(data => data.getBook)
		.json();

	expect(data).toStrictEqual({
		id: '42',
	});
});

test('map single item to class', async () => {
	const response = await client.request(gql`
		query {
			getBook(id: "42") {${allFields(Book)}}
		}
	`);

	const data = response
		.map(data => data.getBook)
		.result<Book, Book>(Book);

	expect(data).toBeInstanceOf(Book);
	expect(data.id).toEqual('42');
	expect(data.title).toEqual('Book_title');
	expect(data.createdAt).toEqual(APP_DATE);
	expect(data.author).toBeUndefined();
	expect(data.chapters).toBeUndefined();
});

test('map multiple items to classes', async () => {
	const response = await client.request(gql`
		query {
			getBooks {${allFields(Book)}}
		}
	`);

	const data = response
		.map(data => data.getBooks)
		.result<Book, Array<Book>>(Book);

	expect(data).toHaveLength(2);
	expect(data[0]).toBeInstanceOf(Book);
	expect(data[0].id).toEqual('42');
	expect(data[0].title).toEqual('Book_title');
	expect(data[0].createdAt).toEqual(APP_DATE);
	expect(data[0].author).toBeUndefined();
	expect(data[0].chapters).toBeUndefined();
	expect(data[1]).toBeInstanceOf(Book);
	expect(data[1].id).toEqual('42');
	expect(data[1].title).toEqual('Book_title');
	expect(data[1].createdAt).toEqual(APP_DATE);
	expect(data[1].author).toBeUndefined();
	expect(data[1].chapters).toBeUndefined();
});

test('map single item with relationships to class', async () => {
	const response = await client.request(gql`
		query {
			getBook(id: "42") {${allFields(Book, ['author.role.users', 'author.books', 'chapters.comments.author'])}}
		}
	`);

	const data = response
		.map(data => data.getBook)
		.result<Book, Book>(Book);

	expect(data).toEqual(createSuperBook());
});

test('map multiple items with relationships to classes', async () => {
	const response = await client.request(gql`
		query {
			getBooks {${allFields(Book, ['author.role.users', 'author.books', 'chapters.comments.author'])}}
		}
	`);

	const data = response
		.map(data => data.getBooks)
		.result<Book, Array<Book>>(Book);

	expect(data).toEqual([
		createSuperBook(),
		createSuperBook(),
	]);
});

test('map multiple variable lists', async () => {
	const varsGetUsers = variables<GetUsersVariables>({
		page: {definition: 'Int'},
		limit: {definition: 'Int'},
	}, {
		page: 2,
	});

	const varsGetBook = variables<GetBookVariables>({
		id: {definition: 'ID!'},
	}, {
		id: '42',
	});

	const data = await client.request(gql`
		query(${varsGetUsers.toDeclarations()}, ${varsGetBook.toDeclarations()}) {
			getBook(${varsGetBook.toArguments()}) {id}
			getUsers(${varsGetUsers.toArguments()}) {id}
		}
	`).then(response => response.json());

	expect(data).toEqual({
		getBook: {id: '42'},
		getUsers: [
			{id: '42'},
			{id: '42'},
		],
	});
});

test('map variables list with custom variables', async () => {
	const varsGetUsers = variables<GetUsersVariables>({
		page: {definition: 'Int'},
		limit: {definition: 'Int'},
	}, {
		page: 2,
	});

	const data = await client.request(gql`
		query(${varsGetUsers.toDeclarations()}, $book: ID!) {
			getBook(id: $book) {id}
			getUsers(${varsGetUsers.toArguments()}) {id}
		}
	`, {
		variables: {
			book: '42',
		},
	}).then(response => response.json());

	expect(data).toEqual({
		getBook: {id: '42'},
		getUsers: [
			{id: '42'},
			{id: '42'},
		],
	});
});

function createSuperBook()
{
	return Book.create(
		'42',
		'Book_title',
		APP_DATE,
		{amount: 1000, currency: 'CZK'},
		User.create(
			'42',
			'john@doe',
			Role.create(
				'42',
				'guest',
				[
					User.create('42', 'john@doe'),
					User.create('42', 'john@doe'),
				],
			),
			[
				Book.create('42', 'Book_title', APP_DATE, {amount: 1000, currency: 'CZK'}),
				Book.create('42', 'Book_title', APP_DATE, {amount: 1000, currency: 'CZK'}),
			],
		),[
			Chapter.create(
				'42',
				'Chapter_title',
				[
					Comment.create(
						'42',
						'Hello World',
						User.create('42', 'john@doe'),
					),
					Comment.create(
						'42',
						'Hello World',
						User.create('42', 'john@doe'),
					),
				],
			),
			Chapter.create(
				'42',
				'Chapter_title',
				[
					Comment.create(
						'42',
						'Hello World',
						User.create('42', 'john@doe'),
					),
					Comment.create(
						'42',
						'Hello World',
						User.create('42', 'john@doe'),
					),
				],
			),
		],
	);
}
