import {mockServer} from 'graphql-tools';

import {HttpClient, HttpRequest} from '../src';
import {APP_MOCKS, APP_SCHEMA} from './schema';


export class MockHttpClient implements HttpClient
{
	private static readonly server = mockServer(APP_SCHEMA, APP_MOCKS, false);

	public async request(request: HttpRequest): Promise<string>
	{
		const data = JSON.parse(request.body);
		const response = await MockHttpClient.server.query(data.query, data.variables);

		return JSON.stringify(response);
	}
}
