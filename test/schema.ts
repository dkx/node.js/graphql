import {IMocks} from '@graphql-tools/mock/types';

import {Configuration, FieldFactory, RelationshipFactory, RelationshipInclusion, TypeFactory} from '../src';


export declare interface Money
{
	amount: number,
	currency: string,
}

class MoneyFactory implements TypeFactory<Money>
{
	public create(data: any, relationshipFactory: RelationshipFactory<Money>): Money
	{
		return {
			amount: data.amount,
			currency: data.currency,
		};
	}
}

export class Book
{
	public id!: string;

	public title!: string;

	public createdAt!: Date;

	public price!: Money;

	public author!: User;

	public chapters!: Array<Chapter>;

	public static create(id: string, title: string, createdAt: Date, price: Money, author?: User, chapters?: Array<Chapter>): Book
	{
		const book = new Book();
		book.id = id;
		book.title = title;
		book.createdAt = createdAt!;
		book.price = price;
		book.author = author!;
		book.chapters = chapters!;

		return book;
	}
}

export class Chapter
{
	public id!: string;

	public title!: string;

	public comments!: Array<Comment>;

	public static create(id: string, title: string, comments?: Array<Comment>): Chapter
	{
		const chapter = new Chapter();
		chapter.id = id;
		chapter.title = title;
		chapter.comments = comments!;

		return chapter;
	}
}

export class Comment
{
	public id!: string;

	public text!: string;

	public author!: User;

	public static create(id: string, text: string, author?: User): Comment
	{
		const comment = new Comment();
		comment.id = id;
		comment.text = text;
		comment.author = author!;

		return comment;
	}
}

export class User
{
	public id!: string;

	public email!: string;

	public role!: Role;

	public books!: Array<Book>;

	public static create(id: string, email: string, role?: Role, books?: Array<Book>): User
	{
		const user = new User();
		user.id = id;
		user.email = email;
		user.role = role!;
		user.books = books!;

		return user;
	}
}

export class Role
{
	public id!: string;

	public name!: string;

	public users!: Array<User>;

	public static create(id: string, name: string, users?: Array<User>): Role
	{
		const role = new Role();
		role.id = id;
		role.name = name;
		role.users = users!;

		return role;
	}
}

class DateFactory implements FieldFactory<Date, string>
{
	public transform(value: string): Date
	{
		return new Date(value);
	}
}

function createConfiguration(): Configuration
{
	const configuration = new Configuration();

	const user = configuration.addType(User);

	const money = configuration.addArbitraryType(new MoneyFactory())
		.addField('amount')
		.addField('currency');

	const comment = configuration.addType(Comment)
		.addField('id')
		.addField('text')
		.addRelationship('author', user);

	const chapter = configuration.addType(Chapter)
		.addField('id')
		.addField('title')
		.addRelationship('comments', comment);

	const book = configuration.addType(Book)
		.addField('id')
		.addField('title')
		.addField('createdAt', {factory: new DateFactory()})
		.addRelationship('price', money, {include: RelationshipInclusion.Always})
		.addRelationship('author', user)
		.addRelationship('chapters', chapter);

	const role = configuration.addType(Role)
		.addField('id')
		.addField('name')
		.addRelationship('users', user);

	user
		.addField('id')
		.addField('email')
		.addRelationship('role', role)
		.addRelationship('books', book);

	return configuration;
}

export const APP_CONFIGURATION: Configuration = createConfiguration();

export const APP_SCHEMA = `
schema {
	query: RootQuery
}

type Money {
	amount: Int!,
	currency: String!
}

type Book {
	id: ID!,
	title: String!,
	createdAt: String!,
	author: User!,
	price: Money!,
	chapters: [Chapter!]!,
}

type Chapter {
	id: ID!,
	title: String!,
	comments: [Comment!]!,
}

type Comment {
	id: ID!,
	text: String!,
	author: User!,
}

type User {
	id: ID!,
	email: String!,
	role: Role!,
	books: [Book!]!,
}

type Role {
	id: ID!,
	name: String!,
	users: [User!]!,
}

type RootQuery {
	getBooks: [Book!]!,
	getBook(id: ID!): Book!,
	getUsers(page: Int, limit: Int): [User!]!,
}
`;

export const APP_DATE = new Date();

export const APP_MOCKS: IMocks = {
	ID: () => '42',
	Money: () => ({
		amount: 1000,
		currency: 'CZK',
	}),
	Book: () => ({
		title: 'Book_title',
		createdAt: APP_DATE.toISOString(),
	}),
	User: () => ({
		email: 'john@doe',
	}),
	Role: () => ({
		name: 'guest',
	}),
	Chapter: () => ({
		title: 'Chapter_title',
	}),
};

export declare interface GetUsersVariables
{
	page?: number,
	limit?: number,
}

export declare interface GetBookVariables
{
	id: string,
}
