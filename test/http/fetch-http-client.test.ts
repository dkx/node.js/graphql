import {enableFetchMocks} from 'jest-fetch-mock';

import {FetchHttpClient, HttpRequest} from '../../src';


enableFetchMocks();

let http: FetchHttpClient;

beforeEach(() => {
	fetchMock.mockResponse(request => request.text());
	http = new FetchHttpClient();
});

test('test fetch http client', async () => {
	const data = await http.request(new HttpRequest('https://localhost', '{"number": 42}'));
	expect(data).toStrictEqual('{"number": 42}');
});
