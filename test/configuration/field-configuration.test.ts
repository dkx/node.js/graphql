import {FieldConfiguration} from '../../src/configuration';


test('configure field with default definition', () => {
	const config = new FieldConfiguration('id');

	expect(config.name).toBe('id');
	expect(config.definition).toBe('id');
});

test('configure field with custom definition', () => {
	const config = new FieldConfiguration('id', {
		definition: 'ID',
	});

	expect(config.name).toBe('id');
	expect(config.definition).toBe('ID');
});

class Book
{
	public readonly id!: string;
}
