import {TypedObjectConfiguration} from '../../src';


let config: TypedObjectConfiguration<Book>;

beforeEach(() => {
	config = new TypedObjectConfiguration<Book>(Book);
})

test('add fields', () => {
	expect(config.fields.size).toBe(0);

	config.addField('id');
	config.addField('title');

	expect(config.fields.size).toBe(2);
});

class Book
{
	public readonly id!: string;

	public readonly title!: string;
}
