import {RelationshipConfiguration, TypedObjectConfiguration} from '../../src/configuration';


test('configure relationship with default definition', () => {
	const config = new RelationshipConfiguration<User>(new TypedObjectConfiguration<User>(User), 'author');

	expect(config.name).toBe('author');
	expect(config.definition).toBe('author');
});

test('configure relationship with custom definition', () => {
	const config = new RelationshipConfiguration<User>(new TypedObjectConfiguration<User>(User), 'author', {
		definition: 'AUTHOR',
	});

	expect(config.name).toBe('author');
	expect(config.definition).toBe('AUTHOR');
});

class User {}
