import {Configuration, TypedObjectConfiguration} from '../../src';


let config: Configuration;

beforeEach(() => {
	config = new Configuration();
});

test('throw an error when type is not configured', () => {
	expect(() => {
		config.getType(Book);
	}).toThrow('GraphQL: missing configuration for type Book');
});

test('add and get configured type', () => {
	config.addType(Book);
	const bookConfig = config.getType(Book);

	expect(bookConfig).toBeInstanceOf(TypedObjectConfiguration);
	expect(bookConfig.type).toBe(Book);
});

class Book {}
