import {Configuration, variables, RequestContext} from '../../src';


declare interface ListVariables
{
	page?: number,
	limit?: number,
	query?: string,
}

let ctx: RequestContext;

beforeEach(() => {
	ctx = new RequestContext(new Configuration());
});

test('print only present variables', () => {
	const vars = variables<ListVariables>({
		page: {definition: 'Int'},
		limit: {definition: 'Int'},
		query: {definition: 'String'},
	}, {
		limit: 100,
		query: 'car',
	});

	expect(vars.toDeclarations().process(ctx)).toBe('$p0: Int, $p1: String');
	expect(vars.toArguments().process(ctx)).toBe('limit: $p0, query: $p1');
});

test('print variables with custom names', () => {
	const vars = variables<ListVariables>({
		page: {name: 'page', definition: 'Int'},
		limit: {name: 'limit', definition: 'Int'},
		query: {definition: 'String'},
	}, {
		page: 2,
		limit: 100,
		query: 'car',
	});

	expect(vars.toDeclarations().process(ctx)).toBe('$page: Int, $limit: Int, $p2: String');
	expect(vars.toArguments().process(ctx)).toBe('page: $page, limit: $limit, query: $p2');
});

test('throw an error when toArguments() is called before toDeclarations()', () => {
	const vars = variables<ListVariables>({
		page: {definition: 'Int'},
		limit: {definition: 'Int'},
		query: {definition: 'String'},
	}, {
		limit: 100,
		query: 'car',
	});

	expect(() => {
		vars.toArguments().process(ctx);
	}).toThrow('VariablesSnippet: toArguments() must be called after toDeclaration()');
});
