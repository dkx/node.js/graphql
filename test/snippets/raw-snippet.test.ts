import {Configuration, raw, RequestContext} from '../../src';


let ctx: RequestContext;

beforeEach(() => {
	ctx = new RequestContext(new Configuration());
});

test('return provided string', () => {
	expect(raw('42').process(ctx)).toBe('42');
});
