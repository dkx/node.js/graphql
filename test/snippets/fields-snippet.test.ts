import {allFields, RequestContext} from '../../src';
import {APP_CONFIGURATION, Book, User} from '../schema';


let ctx: RequestContext;

beforeEach(() => {
	ctx = new RequestContext(APP_CONFIGURATION);
});

test('print all type fields', () => {
	expect(allFields(User).process(ctx))
		.toBe('id, email');
});

test('print all type fields with relationship', () => {
	expect(allFields(User, ['role']).process(ctx))
		.toBe('id, email, role {id, name}');
});

test('print all type fields with nested relationships', () => {
	expect(allFields(Book, ['author.role.users', 'author.books', 'chapters.comments.author']).process(ctx))
		.toBe('id, title, createdAt, price {amount, currency}, author {id, email, role {id, name, users {id, email}}, books {id, title, createdAt, price {amount, currency}}}, chapters {id, title, comments {id, text, author {id, email}}}');
});

test('always include relationship', () => {
	expect(allFields(Book).process(ctx))
		.toBe('id, title, createdAt, price {amount, currency}');
});
