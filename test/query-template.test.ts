import {Configuration, gql, variables, Snippet, RequestContext} from '../src';
import {GetBookVariables, GetUsersVariables} from './schema';


let ctx: RequestContext;

beforeEach(() => {
	ctx = new RequestContext(new Configuration());
});

test('process string only template', () => {
	const template = gql`user {${'id'}, ${'email'}}`;
	expect(template.process(ctx)).toBe(`user {id, email}`);
});

test('process template with snippets', () => {
	const template = gql`user {${new TestSnippet('id')}, ${new TestSnippet('email')}}`
	expect(template.process(ctx)).toBe(`user {id, email}`);
});

test('process template with multiple variables lists', async () => {
	const varsGetUsers = variables<GetUsersVariables>({
		page: {definition: 'Int'},
		limit: {definition: 'Int'},
	}, {
		limit: 100,
	});

	const varsGetBook = variables<GetBookVariables>({
		id: {definition: 'ID!'},
	}, {
		id: '42',
	});

	const template = gql`
		query(${varsGetUsers.toDeclarations()}, ${varsGetBook.toDeclarations()}) {
			getUsers(${varsGetUsers.toArguments()}) {id}
			getBook(${varsGetBook.toArguments()}) {id}
		}
	`;

	expect(template.process(ctx)).toBe(`
		query($p0: Int, $p1: ID!) {
			getUsers(limit: $p0) {id}
			getBook(id: $p1) {id}
		}
	`);

	expect(ctx.variables.size).toBe(2);
	expect(ctx.variables.get('p0')).toBe(100);
	expect(ctx.variables.get('p1')).toBe('42');
});

class TestSnippet extends Snippet
{
	constructor(
		private readonly str: string,
	) {
		super();
	}

	public process(ctx: RequestContext): string
	{
		return this.str;
	}
}
